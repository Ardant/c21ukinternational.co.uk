<?php
/*
Plugin Name: Ardant WP
Description: Ardant Wordpress Plugin.
Author: Daniel Smith
Author URI: https://ardant.co.uk
Version: 1.0.0
Plugin URI: https://ardant.co.uk/
Copyright: Daniel Smith
Text Domain: ardant-wp
*/

function country_init() {
	register_taxonomy(
		'country',
		'post',
		array(
			'label' => __( 'Country' )
		)
	);
}
add_action( 'init', 'country_init' );

function cpt_property() {
	register_post_type( 'property', array(
	  'labels' => array(
	    'name' => 'C21 Properties',
	    'singular_name' => 'Property',
	   ),
	  'description' => 'Custom post type for properties',
	  'taxonomies' => array('country'),
	  'public' => true,
	  'menu_position' => 10,
	  'supports' => array( 'title','editor')
	));
}
add_action( 'init', 'cpt_property' );

function cpt_people() {
	register_post_type( 'people', array(
	  'labels' => array(
	    'name' => 'C21 People',
	    'singular_name' => 'Person',
	   ),
	  'description' => 'Custom post type for people',
	  'public' => true,
	  'menu_position' => 11,
	  'supports' => array( 'title','editor')
	));
}
add_action( 'init', 'cpt_people' );

function cpt_agents() {
	register_post_type( 'agents', array(
	  'labels' => array(
	    'name' => 'C21 Agents',
	    'singular_name' => 'Agent',
	   ),
	  'description' => 'Custom post type for agents',
	  'public' => true,
	  'menu_position' => 11,
	  'supports' => array( 'title','editor','thumbnail')
	));
}
add_action( 'init', 'cpt_agents' );
?>