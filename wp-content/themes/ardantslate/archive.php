<?php get_header(); ?>
<div id="slick" class="slick">
<div><img src="/wp-content/uploads/2017/06/germany3.jpg"></div>
<div><img src="/wp-content/uploads/2017/06/area4.jpg"></div>
<div><img src="/wp-content/uploads/2017/06/germany2.jpg"></div>
<div><img src="/wp-content/uploads/2017/06/germany1.jpg"></div>
</div>
<section id="content" role="main">
<header class="header yellowbg">
<div class="container propertyheader">
<h1 style="padding: 0rem 0;" class="entry-title"><?php 
if ( is_day() ) { printf( __( 'Daily Archives: %s', 'ardantslate' ), get_the_time( get_option( 'date_format' ) ) ); }
elseif ( is_month() ) { printf( __( 'Monthly Archives: %s', 'ardantslate' ), get_the_time( 'F Y' ) ); }
elseif ( is_year() ) { printf( __( 'Yearly Archives: %s', 'ardantslate' ), get_the_time( 'Y' ) ); }
else { echo single_cat_title( '', false ); }
?></h1>
<h2>The following opportunities are available</h2>
</div>
</header>
<div class="container">
<div class="row">

<div class="col-xs-12 col-sm-6"><h2>INVESTMENT SECTION</h2><p>Content here for investment section. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quid iudicant sensus? Sint ista Graecorum; Duo Reges: constructio interrete. At enim hic etiam dolore. Sint modo partes vitae beatae. Stoici scilicet. Summae mihi videtur inscitiae.</p>

<p>Quo igitur, inquit, modo? Respondeat totidem verbis. Huius, Lyco, oratione locuples, rebus ipsis ielunior. Tanta vis admonitionis inest in locis; Sed nimis multa. Quo igitur, inquit, modo? Respondeat totidem verbis. Huius, Lyco, oratione locuples, rebus ipsis ielunior. Tanta vis admonitionis inest in locis; Sed nimis multa.</p></div>
<div class="col-xs-12 col-sm-6">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <?php get_template_part( 'entry-propertylist' ); ?>
    <?php endwhile; endif; ?>
</div>
</div>
<hr/>
<div class="row">
<div class="col-xs-12">
<h2>RELOCATION SECTION</h2>
<p>Call to action here to capture client details</p>
<hr/>
<h2>LIFESTYLE SECTION</h2>
<p>Content here for lifestyle</p>
</div></div><!--row-->
<?php get_template_part( 'nav', 'below' ); ?>

</div>
</section>
<?php get_sidebar(); ?>
<?php get_footer(); ?>