<?php get_header(); ?>
<div id="slick" class="slick">
<?php $images = get_field('slideshow');
    if( $images ): ?>
		<?php foreach( $images as $image ): ?>
            <div>
	            <img src="<?php echo $image['sizes']['large']; ?>" alt=""/>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
<section id="content" role="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php get_template_part( 'entry-property' ); ?>
<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
<?php endwhile; endif; ?>
<footer class="footer">
<?php get_template_part( 'nav', 'below-single' ); ?>
</footer>
<div id="enquirerow">
<div class="container">
	<div class="row">
    	<div class="col-xs-12">
		    <h2>Enquire about this opportunity</h2>
<p>Quo igitur, inquit, modo? Respondeat totidem verbis. Huius, Lyco, oratione locuples, rebus ipsis ielunior. Tanta vis admonitionis inest in locis; Sed nimis multa. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quid iudicant sensus? Sint ista Graecorum; Duo Reges: constructio interrete. At enim hic etiam dolore. Sint modo partes vitae beatae. Stoici scilicet. Summae mihi videtur inscitiae complete the form below.</p>
		</div>
        <section class="col-xs-12 col-sm-4">
        	<h2>Your Century 21  agent</h2>
        	<?php $post_object = get_field('responsible_agent');
			if( $post_object ): 
				// override $post
				$post = $post_object;
				setup_postdata( $post );
				the_post_thumbnail();?>
				<h3><?php the_title(); ?></h3>
                <p class="contact-bit">+44 (0)1234 567 890</p>
                <p class="contact-bit"><a href="mailto:stuart@website.com">stuart@website.com</a></p>
				<p class="contact-linkedin">Connect via <a href="<?php the_field('agent_website'); ?>">LinkedIn</a></p>
				<div class="clearboth agent-details">
                	<?php the_content();?>
                </div>
				<?php wp_reset_postdata();
			endif; ?>
        </section>
		<div class="col-xs-12 col-sm-8">
        	   <?php echo do_shortcode( '[contact-form-7 id="13" title="Contact form 1"]' ); ?>
			   <script>jQuery('#formsubject').val('<?php $subby=get_the_title(); $subby =preg_replace("/&#?[a-z0-9]+;/i","",$subby); echo $subby;?>');</script>	
        </div>        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
 <?php /* <div class="col-xs-12">
         <h2>&nbsp;</h2>
         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quid iudicant sensus? Sint ista Graecorum; Duo Reges: constructio interrete. At enim hic etiam dolore. Sint modo partes vitae beatae. Stoici scilicet. Summae mihi videtur inscitiae.</p>

<p>Quo igitur, inquit, modo? Respondeat totidem verbis. Huius, Lyco, oratione locuples, rebus ipsis ielunior. Tanta vis admonitionis inest in locis; Sed nimis multa.</p>

<p>Quid iudicant sensus? Sint ista Graecorum; Duo Reges: constructio interrete. At enim hic etiam dolore. Sint modo partes vitae beatae. Stoici scilicet. Summae mihi videtur inscitiae.</p>

	<div id="areatn">
            <?php $images = get_field('area_gallery');
                if( $images ): ?>
                    <?php foreach( $images as $image ): ?>
                        <img src="<?php echo $image['sizes']['medium']; ?>" alt=""/>
                    <?php endforeach; ?>
                    <?php foreach( $images as $image ): ?>
                        <img src="<?php echo $image['sizes']['medium']; ?>" alt=""/>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div> 


<p style="clear:both;">Quid iudicant sensus? Sint ista Graecorum; Duo Reges: constructio interrete. At enim hic etiam dolore. Sint modo partes vitae beatae. Stoici scilicet. Summae mihi videtur inscitiae.</p>
*/?>

		</div>
	</div>
</div>
</div>
</section>


<?php get_sidebar(); ?>
<?php get_footer(); ?>