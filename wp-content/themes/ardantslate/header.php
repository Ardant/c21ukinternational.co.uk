<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/bootslate.css" />
<link href="https://fonts.googleapis.com/css?family=Raleway:300,400,700" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri()?>/slick-theme.css">

<link type="text/plain" rel="author" href="/humans.txt" />
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
<div id="opgrey"></div>
<header id="header" role="banner" class="container">
<section id="branding">
<div id="site-title"><?php if ( is_front_page() || is_home() || is_front_page() && is_home() ) { echo '<h1>'; } ?><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_html( get_bloginfo( 'name' ) ); ?>" rel="home">
<img src="/wp-content/themes/ardantslate/img/c21logo.png"/>
</a><?php if ( is_front_page() || is_home() || is_front_page() && is_home() ) { echo '</h1>'; } ?></div>
</section>
<nav id="menu" role="navigation">
<ul id="fake">
	<li><a href="/">Home</a></li>
    <li><a href="/investment">Investment</a></li>
    <li><a href="/relocation">Relocation</a></li>
    <li><a href="/lifestyle">Lifestyle</a></li>
    <li><a href="/about-us">About</a></li>
    <li><a href="/contact">Contact</a></li>
</ul>
<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
</nav>

</header>
<div id="main">