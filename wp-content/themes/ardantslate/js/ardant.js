jQuery('.slick').slick({
  dots: true,
  infinite: true,
  speed: 1000,
  slidesToShow: 1,
  centerMode: true,
  variableWidth: true,
  autoplay: true,
  autoplaySpeed: 4000
});

/* Property Pages */

jQuery('.rowbox1').imagesLoaded().progress( function() {
	rowbox1 = jQuery('.rowbox1').height() + 63;
	jQuery('.rowbox').css({
		'height': rowbox1+'px'
	})
	jQuery('#slickbuilding').slick({
	  dots: true,
	  infinite: true,
      slidesToShow: 1,
	  centerMode: true,
  	  variableWidth: true,
	  speed: 1000,
	  autoplay: true,
	  autoplaySpeed: 4000
	});
});
jQuery(document).on('click touch', '#enquire a.godown', function(event){
    event.preventDefault();
    jQuery('html, body').stop().animate({
        scrollTop: jQuery( jQuery.attr(this, 'href') ).offset().top
    }, 700);
});

jQuery("#map img").on({
 "mouseover" : function() {
    this.src = '/wp-content/themes/ardantslate/img/worldonpng.png';
  },
  "mouseout" : function() {
    this.src = '/wp-content/themes/ardantslate/img/worldpng.png';
  }
});

/* Slider */
function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
    return val;
  }

var slider = document.getElementById('growth_slider');

noUiSlider.create(slider, {
  start: [7],
  connect: [true, false],
  tooltips: true,
  step: 1,
  pips: { // Show a scale with the slider
 	mode: 'values',
	values: [1,15],
    density: 4
  },
  range: {
    'min': 0,
    'max': 16
  },
  format: {
    to: function ( value ) {
    return value + '%';
    },
    from: function ( value ) {
    return value.replace(',-', '');
    }
  }
});

slider.noUiSlider.on('start', function(){
  jQuery( "table" ).fadeTo( "fast" , 0.2, function() {
    // Animation complete.
	  });
	  
});

slider.noUiSlider.on('set', function(){
  // addClassFor(lSet, 'tShow', 450);
  var growth=parseInt(slider.noUiSlider.get());
  //console.log(growth);
  var multiplier = (growth/100)+1;
  //console.log((growth/100)+1); 
  //console.log(multiplier);
  var newVal= multiplier*250000;
  //console.log(newVal);
  if (newVal > 272500) {
	  jQuery('#growth_slider .noUi-connect, .dataBox').animate({backgroundColor:'red'});
  }
  if (newVal < 272501 && newVal > 262500 ) {
	   jQuery('#growth_slider .noUi-connect, .dataBox').animate({backgroundColor:'#efb103'});
  }
  if (newVal < 262501 ) {
	  jQuery('#growth_slider .noUi-connect, .dataBox').animate({backgroundColor:'green'});
  }
  var year1_gain = parseInt(newVal-250000);
  var year2 = parseInt(newVal*multiplier);
  var year2_gain = parseInt(year2-newVal);
  var year3= parseInt(year2*multiplier);
  var year3_gain = parseInt(year3-year2);
  var year4= parseInt(year3*multiplier);
  var year5= parseInt(year4*multiplier);

  var total_gain = year5-250000;
  jQuery('#year1').html(commaSeparateNumber(parseInt(newVal)));
  jQuery('#year1_gain').text(commaSeparateNumber(year1_gain));
  jQuery('#year2').html(commaSeparateNumber(year2));
  jQuery('#year2_gain').text(commaSeparateNumber(year2_gain));

  jQuery('#year3').html(commaSeparateNumber(year3));
  jQuery('#year3_gain').text(commaSeparateNumber(year3_gain));
  jQuery('#year4').html(commaSeparateNumber(year4));
  jQuery('#year5').html(commaSeparateNumber(year5));

  jQuery('#growthtxt').text(growth+'%');
  jQuery('#fiveYearGrowth_pc').text(parseInt((total_gain/250000)*100)+'%');
  jQuery('#fiveYearGrowth').html('£'+commaSeparateNumber(total_gain));
  jQuery( "table" ).fadeTo( "fast" , 1, function() {
    // Animation complete.
  });
});