
<article style="margin-left: 4rem;" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<header>
<?php $images = get_field('slideshow'); $image = $images[0];?>
<a href="<?php the_permalink(); ?>"><img class="slidethumb" src="<?php echo $image['sizes']['medium']; ?>" alt=""/></a>

<?php if ( is_singular() ) { echo '<h1 class="entry-title">'; } else { echo '<h2 class="entry-title">'; } ?><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_title(); ?></a>
<?php if ( is_singular() ) { echo '</h1>'; } else { echo '</h2>'; } ?> <?php edit_post_link(); ?>
<?php //if ( !is_search() ) get_template_part( 'entry', 'meta' ); ?>
</header>
<?php get_template_part( 'entry', ( is_archive() || is_search() ? 'summary' : 'content' ) ); ?>
</article>