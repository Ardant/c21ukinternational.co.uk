<?php /* template name:Sidebar */ get_header(); ?>
<div id="slickhome" class="slick">
<?php $images = get_field('pageslideshow');
    if( $images ): ?>
		<?php foreach( $images as $image ): ?>
            <div>
	            <img src="<?php echo $image['sizes']['large']; ?>" alt=""/>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
<section id="content" role="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<header class="header yellowbg <?php the_title(); ?>bg">
	<div class="container propertyheader">
		<h1 class="entry-title"><?php the_title(); ?></h1> <?php edit_post_link(); ?>
	</div>
</header>
<section class="entry-content container">
	<div class="row">
		<div class="col-xs-12 col-sm-8">
			<?php the_content(); ?><div class="entry-links"><?php wp_link_pages(); ?></div>
		</div>
        <div class="col-xs-12 col-sm-4">
        	<?php // get_sidebar(); ?>
            <h2>Current Investment Opportunities</h2>
            <a href="http://c21ukinternational-co-uk.stackstaging.com/property/berlin-property-1/"><img class="sidebarthumb" src="http://c21ukinternational-co-uk.stackstaging.com/wp-content/uploads/2017/06/camera1-300x200.jpg" alt=""/></a>

<h3 class="entry-title"><a href="http://c21ukinternational-co-uk.stackstaging.com/property/berlin-property-1/" title="Da Vinchi &#8211; Prenzlauer Berg near Mauerpark" rel="bookmark">Da Vinchi &#8211; Prenzlauer Berg near Mauerpark</a>
</h3>
        </div>
	</div><!--row-->
    
<?php if (is_front_page()) { ?>
    <div class="row">
		<div class="col-xs-12">
        <h2 class="maph2">Browse by Country</h2>
        	<div id="map">
            	<a href="/country/germany"><img src="<?php echo get_template_directory_uri()?>/img/worldpng.png" alt=""/></a>
            </div>
        </div>    
    </div>
<?php } /*If home*/?>

</section>
<br/><br/>
<div id="enquirerow">
<div class="container">
	<div class="row">
    	<div class="col-xs-12 col-sm-6">
		    <h2>Call to Action</h2>
<p>Quo igitur, inquit, modo? Respondeat totidem verbis. Huius, Lyco, oratione locuples, rebus ipsis ielunior. Tanta vis admonitionis inest in locis; Sed nimis multa.</p>
		    <?php echo do_shortcode( '[contact-form-7 id="13" title="Contact form 1"]' ); ?>
		</div>
    	<div class="col-xs-12 col-sm-6">
         <h2>&nbsp;</h2>
         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quid iudicant sensus? Sint ista Graecorum; Duo Reges: constructio interrete. At enim hic etiam dolore. Sint modo partes vitae beatae. Stoici scilicet. Summae mihi videtur inscitiae.</p>

<p>Quo igitur, inquit, modo? Respondeat totidem verbis. Huius, Lyco, oratione locuples, rebus ipsis ielunior. Tanta vis admonitionis inest in locis; Sed nimis multa.</p>

<p>Quid iudicant sensus? Sint ista Graecorum; Duo Reges: constructio interrete. At enim hic etiam dolore. Sint modo partes vitae beatae. Stoici scilicet. Summae mihi videtur inscitiae.</p>

			<div id="areatn">
            <?php $images = get_field('area_gallery');
                if( $images ): ?>
                    <?php foreach( $images as $image ): ?>
                        <img src="<?php echo $image['sizes']['medium']; ?>" alt=""/>
                    <?php endforeach; ?>
                    <?php foreach( $images as $image ): ?>
                        <img src="<?php echo $image['sizes']['medium']; ?>" alt=""/>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>


<p style="clear:both;">Quid iudicant sensus? Sint ista Graecorum; Duo Reges: constructio interrete. At enim hic etiam dolore. Sint modo partes vitae beatae. Stoici scilicet. Summae mihi videtur inscitiae.</p>


		</div>
	</div>
</div>
</div>














</article>
<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
<?php endwhile; endif; ?>
</section>

<?php get_footer(); ?>