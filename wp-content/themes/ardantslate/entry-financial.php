<div class="row financials" id="row4">
	<div class="col-xs-12"><h1>Financials</h2>
	    <p class="small">Use your own market knowledge to change our assumptions, or take a look at what can happen with changes in the market.</p>
    </div>
	<div class="col-md-6">
		<h5>Estimated Annual Growth (%)</h5>
		<div id="growth_slider"></div>
			<h5>Estimated 5 Year Growth</h5>
			<div class="dataBox">5 Year Growth<span id="fiveYearGrowth">&pound;100,638</span><span id="fiveYearGrowth_pc">&pound;40%</span> </div>
            <a style="display:block; padding-top: 1rem" href="#">Risk disclosure</a>
		</div>
		<div class="col-md-6">
			<h5>Estimated Values based on <span id='growthtxt'>7%</span> growth per year</h5>
			<table class="table">
                <thead>
                  <th></th>
                  <th>Year Gain (&pound;)</th>
                   <th>Value (&pound;)</th>
                </thead>
                <tr>
                  <td>Year 1</td>
                  <td id="year1_gain" class="number">
                       17,500              </td>
                  <td id="year1"  class="newValue">
                    267,500 
                  </td>
                </tr>
                   <tr>
                  <td>Year 2</td>
                  <td  id="year2_gain" class="number" style="text-align: right;">
                        18,725
                  </td>
                  <td  id="year2"  class="newValue">
                    286,225
                  </td>
                </tr>
                  <tr>
                  <td>Year 3</td>
                  <td   id="year3_gain" class="number">
                        20,036
                  </td>
                  <td id="year3"  class="newValue">
                      306,261              </td>
                </tr>
    
                 <tr>
                  <td>Year 4</td>
                  <td id="year4_gain" class="number">
                        21,438              </td>
                  <td id="year4" class="newValue">
                         327,699              </td>
                </tr>
    
                 <tr>
                  <td>Year 5</td>
                  <td id="year5_gain" class="number">
                    22,939              </td>
                 <td id="year5" class="newValue">
                     350,638             </td>
                </tr>
          </table>
		</div>

	</div>
