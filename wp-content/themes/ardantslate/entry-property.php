<div id="post-<?php the_ID(); ?>" <?php post_class('yellowbg propertyheader'); ?>>
<header class="container">
<div class="row">
	<div class="col-xs-12 col-sm-7">	
		<?php if ( is_singular() ) { echo '<h1 class="entry-title">'; } else { echo '<h2
		 class="entry-title">'; } ?><?php the_title(); ?><?php if ( is_singular() ) { echo '
		 </h1>'; } else { echo '</h2>'; } ?> <?php edit_post_link(); ?>
        <h2><?php echo get_field('subheading');?></h2>
        <?php //if ( !is_search() ) get_template_part( 'entry', 'meta' ); ?>
	</div>
    <div class="col-xs-12 col-sm-5">
    	<div id="enquire">
        	<ul>
            	<li class="godown" style="margin-bottom:1rem;"><a href="#enquirerow">Enquire</a></li>
                <li><a href="/country/germany/">Similar</a></li>
            </ul>
        </div>
    </div>
</div><!--row-->
</header>
</div>
<article class="container">
<div id="row1" class="row">
<?php 
	$location = get_field('location');
	$building = get_field('building');
	$pricing = get_field('pricing');
	$growth = get_field('growth');
	$growth_percentage = get_field('growth_percentage');
	$building_description= get_field('building_description');
	$building_gallery= get_field('building_gallery');
	$area_description= get_field('area_description');
	$map_link= get_field('map_link');
	$area_gallery= get_field('area_gallery');
	$row1 = 0;
	$row1col="";
	if (!empty($location)) {
		$row1++;
	}
	if (!empty($building)) {
		$row1++;
	}
	if (!empty($pricing)) {
		$row1++;
	}
	if (!empty($growth)) {
		$row1++;
	}
	if (!empty($transport)) {
		$row1++;
	}

	if ( $row1 > 4) {
		$transport="";
		$row1 = 4;	
	}
	
	switch($row1) {
			case "2":
				$row1col="col-sm-6";
				break;
			case "3":
				$row1col="col-sm-4";
				break;
			case "4":
				$row1col="col-sm-3";
				break;
	}
	if (!empty($transport)) { ?>
    	<div class="col-xs-12 <?php echo $row1col;?>">  
       		<div class="rowbox rowbox3">
	        <img src="<?php echo get_template_directory_uri()?>/img/transport.png" alt=""/>
        	<h2>Transport</h2>
			<?php echo $transport;?>
            </div>
        </div>  	
    <? }

	if (!empty($location)) { ?>
    	<div class="col-xs-12 <?php echo $row1col;?>"> 
        	<div class="rowbox rowbox1">
        	<img src="<?php echo get_template_directory_uri()?>/img/location.png" alt=""/>
        	<h2>Location</h2>
			<?php echo $location;?>
            </div>
        </div>  	
    <? }

	if (!empty($building)) { ?>
    	<div class="col-xs-12 <?php echo $row1col;?>">  
        	<div class="rowbox rowbox2">
        	<img src="<?php echo get_template_directory_uri()?>/img/building.png" alt=""/>
            <h2>Bulding</h2>
			<?php echo $building;?>
            </div>
        </div>  	
    <? }
	
	if (!empty($pricing)) { ?>
    	<div class="col-xs-12 <?php echo $row1col;?>">  
       		<div class="rowbox rowbox3">
	        <img src="<?php echo get_template_directory_uri()?>/img/currency.png" alt="" title="Click here to switch currencies between &euro; and &pound;"/>
        	<h2>Pricing</h2>
			<?php echo $pricing;?>
            </div>
        </div>  	
    <? }

	if (!empty($growth)) { ?>
    	<div class="col-xs-12 <?php echo $row1col;?>">  
       		<div class="rowbox rowbox3">
	        <img src="<?php echo get_template_directory_uri()?>/img/stock.png" alt=""/>
        	<h2>Investment</h2>
			<?php echo $growth;?>
            <h3><?php echo $growth_percentage;?>%</h3>
            </div>
        </div>  	
    <? }

?>
</div><!--row1-->

<div id="row2" class="row">
	<div class="col-xs-12 col-sm-6">
    	<h2>The Building</h2>
		<?php echo $building_description;?>
	</div>
    <div class="col-xs-12 col-sm-6">
    	<div id="slickbuilding" class="slick">
		<?php $images = get_field('building_gallery');
            if( $images ): ?>
                <?php foreach( $images as $image ): ?>
                    <div>
                        <img src="<?php echo $image['sizes']['large']; ?>" alt=""/>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
		</div>
    </div>
</div>

<?php get_template_part('entry-financial');?>

<div id="row3" class="row">
<div class="col-xs-12 col-sm-6">
    	<h2>The Area</h2>
		<?php echo $area_description;?>
	</div>
    <div class="col-xs-12 col-sm-6">
    	<iframe src="<?php echo $map_link;?>" width="100%" height="300" frameborder="0" allowfullscreen>
        </iframe>
    </div>
    <!--<div class="col-xs-12">
        <div id="slickarea" class="slick">
            <?php $images = get_field('area_gallery');
                if( $images ): ?>
                    <?php foreach( $images as $image ): ?>
                        <div>
                            <img src="<?php echo $image['sizes']['large']; ?>" alt=""/>
                        </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
		</div>-->
</div>



<?php get_template_part( 'entry', ( is_archive() || is_search() ? 'summary' : 'content' ) ); ?>
</article>