<?php get_header(); ?>

<div id="slickhome" class="slick">
<?php $images = get_field('pageslideshow');
    if( $images ): ?>
		<?php foreach( $images as $image ): ?>
            <div>
	            <img src="<?php echo $image['sizes']['large']; ?>" alt=""/>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
<section id="content" role="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<header class="header yellowbg">
	<div class="container propertyheader">
		<h1 class="entry-title"><?php the_title(); ?></h1> <?php edit_post_link(); ?>
	</div>
</header>
<section class="entry-content container">
	<div class="row">
		<div class="col-xs-12">
			<?php the_content(); ?><div class="entry-links"><?php wp_link_pages(); ?></div>
		</div>
	</div><!--row-->
</section>






</article>
<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
<?php endwhile; endif; ?>
</section>

<?php get_footer(); ?>