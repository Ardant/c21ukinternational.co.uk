<?php /* Template name: About Us */get_header(); ?>
<div id="slickhome" class="slick">
<?php $images = get_field('pageslideshow');
    if( $images ): ?>
		<?php foreach( $images as $image ): ?>
            <div>
	            <img src="<?php echo $image['sizes']['large']; ?>" alt=""/>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
<section id="content" role="main">
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
<header class="header yellowbg">
	<div class="container propertyheader">
		<h1 class="entry-title"><?php the_title(); ?></h1> <?php edit_post_link(); ?>
        <h2>We have an extensive team of experts who have decades of experience working within the domestic UK and international markets.  Our two principal directors are Paul Morgan (CEO) and Stuart Pooley (International Sales Director).</h2>
	</div>
</header>
<section class="entry-content container">
	<div class="row">
		<div class="col-xs-12">
			<?php //the_content(); ?><div class="entry-links"><?php wp_link_pages(); ?></div>
		</div>
	</div><!--row-->
</section>
<?php $bg = get_field("people_bg",89);?>
<section id="person" style="background-image:url(<?php echo $bg['url'];?>)">

<div class="container">
	<div class="row">
    	<div class="col-xs-12 col-sm-8 personbox">
        <h1>Paul Morgan</h1>
        <?php	$mypost = get_post(89);
		echo apply_filters('the_content',$mypost->post_content);
		?>
        </div>
    </div>
</div><!--cont-->
</section>
<div class="engagewith">
<div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <h2>Engage with Paul Morgan</h2>
 <?php echo do_shortcode( '[contact-form-7 id="95" title="Contact Paul"]' ); ?>


            </div>
            <div class="col-xs-12 col-sm-6">
                <h2>&nbsp;</h2>
    		<ul style="list-style-type:none;">
            	<li><strong>Phone</strong>: 01234 567890</li>
                <li><strong>Email</strong>: name@email.com</li>
            </ul>
                <script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
    <script type="IN/MemberProfile" data-id="uk.linkedin.com/in/stuart-pooley-b5a98830/" data-format="inline" data-related="false"></script>

            </div>
        </div>
</div><!--cont-->
</div>

<?php $bg = get_field("people_bg",93);?>
<section id="person2" style="background-image:url(<?php echo $bg['url'];?>)">
<div class="container">
	<div class="row">
	    <div class="col-sm-4"></div>
    	<div class="col-xs-12 col-sm-8 personbox2">
        <h1>Stuart Pooley</h1>
        <?php	$mypost = get_post(93);
		echo apply_filters('the_content',$mypost->post_content);
		?>
        </div>
    </div>
</div><!--cont-->
<div class="engagewith">
<div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6">
                <h2>Engage with Stuart Pooley</h2>
 <?php echo do_shortcode( '[contact-form-7 id="95" title="Contact Paul"]' ); ?>


            </div>
            <div class="col-xs-12 col-sm-6">
                <h2>&nbsp;</h2>
    		<ul style="list-style-type:none;">
            	<li><strong>Phone</strong>: 01234 567890</li>
                <li><strong>Email</strong>: name@email.com</li>
            </ul>
                <script src="//platform.linkedin.com/in.js" type="text/javascript"></script>
    <script type="IN/MemberProfile" data-id="uk.linkedin.com/in/stuart-pooley-b5a98830/" data-format="inline" data-related="false"></script>
            </div>
        </div>
</div><!--cont-->
</div>

</section>

</article>
<?php if ( ! post_password_required() ) comments_template( '', true ); ?>
<?php endwhile; endif; ?>
</section>

<?php get_footer(); ?>